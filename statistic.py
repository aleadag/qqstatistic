# -*- coding: utf-8 -*-
from email.encoders import encode_base64
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from qqbot.utf8logger import DEBUG, WARN, INFO
from qqbot.mailagent import MailAgent
from qqbot import QQBotSched as qqbotsched
from qqbot.common import PY3
from datetime import datetime, date, timedelta
import sqlite3
import re
import csv
import io

class g(object):
  conf = { 'groups': [], 'pass_pattern': u'([a-zA-Z]+)\s+ET\s+Song', 'reply': u'{0}，你的歌声真美', 'db_file': './statistic.db', 'cut_time': '14:30' }

def onPlug(bot):
  g.conf.update(bot.conf.pluginsConf.get(__name__, {}))
  DEBUG('ON-PLUG: statistic, conf: %s', str(g.conf))

  g.pass_pattern = re.compile(g.conf['pass_pattern'], re.IGNORECASE)

  if bot.conf.mailAccount:
    g.mail_agent = MailAgent(
      bot.conf.mailAccount, bot.conf.mailAuthCode, name='QQBot管理员'
    )

  g.conn = sqlite3.connect(g.conf['db_file'])
  # Create talbe if it doesn't exit
  c = g.conn.cursor()
  c.execute('''create table if not exists checkin (id INTEGER PRIMARY KEY AUTOINCREMENT, group_name TEXT, checkin_date TEXT, qq TEXT, name TEXT, checkin_time TEXT)''')
  c.execute('''CREATE UNIQUE INDEX if not exists checkin_idx ON checkin(group_name, checkin_date, name)''') # QQ号暂时不可用
  g.conn.commit()

  g.hour, g.minute = g.conf['cut_time'].split(':')
  
  @qqbotsched(hour=g.hour, minute=g.minute)
  def schedRestart(_bot):
      send_reports(_bot)

  # send_reports(bot)
  INFO('已创建计划任务：每天 %s 发送统计报告', g.conf['cut_time'])

def onUnplug(bot):
  DEBUG('ON-UNPLUG: statistic')
  if g.conn:
    g.conn.close()

def onQQMessage(bot, contact, member, content):
  if contact.ctype == 'group' and contact.name in g.conf['groups']:
    match = g.pass_pattern.search(content)
    if match:
      who = match.group(1).lower()
      insert_checkin(who, contact, member)
      bot.SendTo(contact, g.conf['reply'].format(who))

def insert_checkin(who, contact, member):
  now = datetime.now()
  c = g.conn.cursor()
  c.execute('''INSERT OR REPLACE INTO checkin VALUES (NULL, ?, ?, ?, ?, ?)''', (contact.name, str(now.date()), member.name, who, str(now.time())))
  g.conn.commit()

# exclude start_date, include end_date
def daterange(start_date, end_date):
    for n in range(int ((end_date - start_date).days)):
        yield start_date + timedelta(n+1)

def fetch_checkins(db_conn, group, start_date, end_date):
  dates = tuple(daterange(start_date, end_date))
  questionmarks = '?' * len(dates)
  c = db_conn.cursor()
  formatted_query = "SELECT name, checkin_date FROM checkin WHERE group_name=? and checkin_date in ({})".format(','.join(questionmarks))
  query_args = [group]
  query_args.extend(dates)
  c.execute(formatted_query, query_args)
  return c.fetchall()

def rows_to_csv(rows):
  name_to_dates = {}
  all_dates = set()
  for name, date in rows:
    dates = name_to_dates.get(name, set())
    dates.add(date)
    name_to_dates[name] = dates
    all_dates.add(date)
  output = io.StringIO()
  csv_writer = csv.writer(output, delimiter=',', quoting=csv.QUOTE_MINIMAL)
  all_dates = list(all_dates)
  all_dates.sort()
  # Write header
  csv_writer.writerow(['Name'] + all_dates)
  for name, dates in name_to_dates.items():
    marks = [u'Y' if d in dates else u'N' for d in all_dates]
    csv_writer.writerow([name] + marks)
  return output.getvalue()

def send_reports(bot):
  end_date = date.today()
  start_date = end_date - timedelta(7)
  for group in g.conf['groups']:
    rows = fetch_checkins(g.conn, group, start_date, end_date)
    checkin_csv = rows_to_csv(rows)
    mail = {
      'to_addr': bot.conf.mailAccount,
      'subject': ('%s-%s [%s, %s]' % ('打卡统计', group, start_date, end_date)),
      'to_name': '我'
    }
    try:
      with MySmtp(g.mail_agent) as smtp:
        smtp.send(csv_content=checkin_csv, **mail)
    except Exception as e:
        WARN('无法将统计结果发送至邮箱%s %s', g.mail_agent.account, e, exc_info=True)
    else:
        INFO('已将统计结果发送至邮箱%s', g.mail_agent.account)

class MySmtp(object):
    def __init__(self, mail_agent):
        self.name, self.account = mail_agent.name, mail_agent.account
        self.server = mail_agent.st_SMTP()
        try:
            self.server.login(mail_agent.account, mail_agent.auth_code)
        except:
            self.close()
            raise
    
    def close(self):
        try:
            return self.server.quit()
        except:
            pass
    
    def __enter__(self):
        return self
    
    def __exit__(self, exc_type, exc_value, traceback):
        self.close()
            
    def send(self, to_addr, html='', subject='', to_name='', csv_content=''):
        subject = subject or 'No subject'
        to_name = to_name or to_addr.split('@')[0]
        html = '<html><body>%s</body></html>' % html
        if html:
            html = html.replace('{{png}}', '<img src="cid:0">')

        msg = MIMEMultipart()
        msg.attach(MIMEText(html, 'html', 'utf8'))
        if not PY3:
            msg['From'] = format_addr(self.name)
            msg['To'] = format_addr('%s <%s>' % (to_name, to_addr))
            msg['Subject'] = Header(subject.decode('utf8'), 'utf8').encode()
        else:
            msg['From'] = self.name
            msg['To'] = '%s <%s>' % (to_name, to_addr)
            msg['Subject'] = subject    
        
        if csv_content:
            m = MIMEBase('text', 'csv', filename='checkins.csv')
            m.add_header('Content-Disposition', 'attachment', filename='checkins.csv')
            m.add_header('Content-ID', '<0>')
            m.add_header('X-Attachment-Id', '0')
            m.set_payload(csv_content)
            encode_base64(m)        
            msg.attach(m)
        
        self.server.sendmail(self.account, to_addr, msg.as_string())

if __name__ == '__main__':
    from qqbot.qconf import QConf

    conf = QConf(['-u', 'awang'])
    conf.Display()
