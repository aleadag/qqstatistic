# -*- coding: utf-8 -*-
import sqlite3
from datetime import date
from statistic import fetch_checkins, rows_to_csv

def test_fetch_checkins():
  db_conn = sqlite3.connect('test.db')
  start_date = date(2018, 8, 9)
  end_date = date(2018, 8, 17)
  rows = fetch_checkins(db_conn, u'测试群', start_date, end_date)
  print(rows)
  assert False

def test_rows_to_csv():
  rows = [('rocky', '2018-08-15'), ('rocky', '2018-08-16'), ('tom', '2018-08-12')]
  csv = rows_to_csv(rows)
  print(csv)
